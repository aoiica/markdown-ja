# README


## リンク
* [技術文書翻訳のための慣用句集](https://piyo-ko.github.io/en/index.html)
* [軽量マークアップ言語の歴史](http://d.hatena.ne.jp/kk_Ataka/20150713/1436751705)
* [軽量マークアップ言語の比較](http://hyperpolyglot.org/lightweight-markup)
* [commonmark.org](https://commonmark.org)
    * [最新のドキュメント](https://spec.commonmark.org/0.28/)
* [GFM](https://github.github.com/gfm/)
* [en-wikipe-markdown](https://en.wikipedia.org/wiki/Markdown)
* [higumaさんによるmarkdownチートシート](https://github.com/higuma/markdown_cheat_sheet)
    * チートシートを名乗っているが、事実上のオリジナルドキュメントの翻訳であり、解説もついているし、GFMとQiita拡張も論じている。もうこれがあれば十分だと思う。
* [Markdown日本語対応星取表 —— 強調箇所は分かち書き必須か](https://qiita.com/8x9/items/19acf458ba04b25a30e4)

## 用語対応表
Mozillaの発行する文書を参考にした

* spec = specification = 仕様書
* attribute = 属性
* element = 要素
    * block-level HTML elements = ブロック要素/ブロックレベル要素
    * span-level HTML elements = インライン要素  <なぜこうなったのか
* content = 内容
* tag = ダグ
    * Opening tag = start tag = 開始タグ
    * Closing tag = 終了タグ  <閉じタグの方が好き
    * 開始タグ　<=> 閉じタグ <はどうか
* 山括弧
* Mozillaの訳者注: HTML エンティティ、!実体参照、文字参照、文字実体参照、は、それほぼ同じ意味で用いられます。
* e.g. = 例として
* for example = たとえば
* literal characters = ただの文字 / そのままの文字
* email = メール
* もしくは = 片方１つのみが選択できる状況
* または = 両方同時に選択も可能な状況
* space = 空白
* underscore = 下線符号 <あまり馴染みがない
* article = 記事?、!文章
* document = 文書
* syntax = 構文規則・構文 <論じる対象によって使い分ける
* add = 書き込む・記入・!追加・書く
* reference-style = 脚注形式
* + = !プラス記号・加算記号
* - = !マイナス記号・除算記号・ハイフン
* ! = !感嘆符・雨垂れ・エクスクラメーション <雨垂れいいですね
* _ = アンダースコア・下線符号
* ` ` = !バッククオート・バックティック
* * = アスタリスク・アステリスク・星号・星印・星・アスタ
* & = アンパサンド・!アンド記号
* -- = （全角かっこ）
* (半角かっこ) = (そのまま)
* source = !ソース・設計図 <断じて設計図ではない
* 使う・用いる <用いるの方が硬め、間をとって使用するは無い
* easy = 容易い・!たやすい・容易に・よういに・難なく・手軽に・造作無く　<簡単に楽いやだ
* note = 注意する
* Markdown formatting syntax = マークダウンを翻訳する構文規則
* must/need to = しなくてはならない・しなくではいけない・する必要がある  <"しなくては"は二重否定なので嫌い、する必要があるは文字的に空間の威圧感があって嫌だ、ので適宜混ぜて使う
* "次の"を補う
* support = 提供する
* list markers = 行頭記号
* want = 必要であれ
* Code Block = コードブロック
* translate = 翻訳・変換 <翻訳の方が響きがいいのだが、wikipeで"翻訳"と訳すのはnoobのやることだと酷評されているのでどうしようか
* escape = 無効化・エスケープ
