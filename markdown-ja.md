マークダウン: 構文
=============
{**訳者注** この文書は歴史的価値を有するが、この文書を実務の参考にすることについて、現在は[コモンマーク](https://commonmark.org)や[GFM](https://github.github.com/gfm/)が策定の中心となっているため必ずしも推奨されない。}
<ul id="ProjectSubmenu">
    <li><a href="/projects/markdown/" title="markdown Project Page">Main</a></li>
    <li><a href="/projects/markdown/basics" title="markdown Basics">Basics</a></li>
    <li><a class="selected" title="markdown Syntax Documentation">Syntax</a></li>
    <li><a href="/projects/markdown/license" title="Pricing and License Information">License</a></li>
    <li><a href="/projects/markdown/dingus" title="Online markdown Web Form">Dingus</a></li>
</ul>
{**訳者注** 上記のリンクは有効ではありません}

* [概要](#overview)
  * [哲学](#philosophy)
  * [HTMLの記入](#html)
  * [自動実体参照](#autoescape)
* [ブロック要素](#block)
  * [段落と改行](#p)
  * [見出し](#header)
  * [引用](#blockquote)
  * [リスト](#list)
  * [ブロックのコード](#precode)
  * [水平罫線](#hr)
* [インライン要素](#span)
  * [リンク](#link)
  * [強調](#em)
  * [インラインのコード](#code)
  * [画像](#img)
* [その他](#misc)
  * [自動リンク](#autolink)
  * [バックスラッシュエスケープ](#backslash)


**注意** この文書はそれ自体がマークダウンで書かれている。[URLに'.text'をつけることでそのソースを見ることができる。][src]{**訳者注** 右記のリンクは有効ではありません}

  [src]: /projects/markdown/syntax.text

* * *

<h2 id="overview">概要</h2>

<h3 id="philosophy">哲学</h3>

マークダウンは可能な限り書きやすくあれ読みやすくあれということが意図されている。

しかしながら、読みやすさが他の何よりも強調されるだろう。マークダウンで書かれた文書はプレーンテキストと同じようにそのまま公開できるべきで、タグなどの書式指示要素によってマークアップされた見た目であるべきではない。もちろんマークダウンの構文は、[Setext][1]、[atx][2]、[Textile][3]、[eStructuredText][4]、[Grutatext][5]、[EtText][6] などのテキストをHTMLに変換する既存の変換器のいくつかより影響を受けた。しかし一番大きな影響を受けたのはプレーンテキストを用いるメールの構文だ。

  [1]: http://docutils.sourceforge.net/mirror/setext.html
  [2]: http://www.aaronsw.com/2002/atx/
  [3]: http://textism.com/tools/textile/
  [4]: http://docutils.sourceforge.net/rst.html
  [5]: http://www.triptico.com/software/grutatxt.html
  [6]: http://ettext.taint.org/doc/

この章の終わりにあたり、マークダウンの構文は例外なく句読点等の普遍的な記号から構成されており、これらの記号が何を意味するか直感的に把握できるよう注意深く選出されたことを付け加える。例として、単語の両端にあるアスタリスクは \*強調\* と認識することができる。マークダウンのリストも、まさにリスト以外の何者でもないと認識できる。ブロッククオートでさえも本文中の引用であると認識できる、これまでメールで使ってきたように。



<h3 id="html">HTMLの記入</h3>

マークダウンの構文規則は一つの目的を持っている。ウェブで *書く* ために使われる書式となることだ。

マークダウンはHTMLを置き換えるものではなく、それに似せようとするものでもない。構文の種類はとても少なく、HTMLのごく一部のタグとしか対応していない。狙いはHTMLをより簡単に挿入すること *ではない* 。私の見解では、HTMLタグはすでに容易く挿入ができる。マークダウンの狙いは、散文をたやすく読み書き編集することだ。すなわちHTMLは *公開* のための書式であり、マークダウンは *書く* ための書式であるということだ。それゆえに、マークダウンを翻訳する構文規則は、プレーンテキストによって伝達できることにのみ焦点を当てている。

マークダウンの構文に含まれていないその他のマークアップについては、HTMLをそのまま使えばよい。マークダウンからHTMLに切り替えることを示すための宣言は必要なく、ただタグを書くだけで十分である。

制限されるのはHTMLのブロック要素のみであり（例として`<div>`、`<table>`、`<pre>`、`<p>` などがある）これらは空行によって前後の要素と区別する必要があり、開始終了タグはタブまたは空白を使ってインデントしてはならない。マークダウンを使うときに`<p>`タグをHTMLブロックタグの周りに追加する必要はない。

たとえば、マークダウンで書かれた文章にHTMLのテーブルを記入する場合：

    ここまでが標準の段落です。

    <table>
        <tr>
            <td>ほげ</td>
        </tr>
    </table>

    ここからは別の標準段落です。

注意すべきことに、マークダウンを翻訳する構文規則はHTMLのブロックタグの内側での処理を行わない。例として、HTMLのブロック要素の内側ではマークダウンの`*強調*`を利用することができない。

HTMLのインラインタグは（例として`<span>`、`<cite>`、`<del>` などは）マークダウンで書かれた、文章、リスト、ヘッダ のいずれの位置においても利用することができる。必要であれば、HTMLタグをマークダウンの書式の代わりに使うことすらできる。たとえばHTMLの`<a>`や`<img>`タグを使うことをマークダウンのリンクや画像の構文を使うことよりも好むのであれば、そのようにすればよい。

HTMLのブロックタグとは違い、*マークダウンの構文は* インラインタグの中で処理が行われる。


<h3 id="autoescape">自動実体参照</h3>

HTMLにおいて、特別な扱いをすべき文字が２つある、`<`と`&`だ。左窄山括弧は開始タグに使われ、アンド記号は実体参照の存在を示す。もしこれらをただの文字として使いたいならば、実体参照をしなければならない、例として`&lt;`や`&amp;`など。

紛れ込むアンド記号はウェブライターを疲弊させている。もし'AT&T'と表示したいならば、'`AT&amp;T`'と書かなくてはならない。URLの中においてすらアンド記号を代替する必要がある。したがって、次のようにリンクを貼りたいとしたら：

    http://images.google.com/images?num=30&q=larry+bir

URLを次のように符号化しなくてはいけない：

    http://images.google.com/images?num=30&amp;q=larry+bird

`href`属性のアンカータグの中において。言及する必要はないが、よく忘れられ、おそらく十分に構成されたマークアップウェブサイトにおけるもっとも一般的なHTML構造解析時のエラーの原因ではないだろうか。

マークダウンはこれらの文字を自然に使うことを可能にし、必要な実体参照を全て引き受ける。もしアンド記号を実体参照の一部として使えば変更せずに保護され、その他のところでは`&amp;`に翻訳される。

そのようなわけで、記事にコピーライトシンボルを含めたいと考えるときは、このように描けばよい：

    &copy;

そしてマークダウンはこれを放置する。しかし次のように描けば：

    AT&T

マークダウンはこれを翻訳する：

    AT&amp;T

同様に、マークダウンは[inline HTML](#html)を提供しているため、山括弧をHTMLタグの区切り文字として使えば、マークダウンはそのように扱う。しかし次のように書けば：

    4 < 5

マークダウンはこれを翻訳する：

    4 &lt; 5

しかしながら、マークダウンにおけるブロック要素とインライン要素の中では、山括弧とアンド記号は *必ず* 自動的に変換される。これはマークダウンを用いてHTMLコードを書くことを容易にする。(対してそのままのHTMLは、HTMLの構文を表現することに対して最悪の方法であり、なぜならすべての`<`と`&`を代替しなければならないからだ。)


* * *


<h2 id="block">ブロック要素</h2>


<h3 id="p">段落と改行</h3>

ある段落は簡単に言えば１つ以上の連続する行からなり、１つ以上の空行によって区別される。（空行とは空行に見える全ての行を指す。すなわち空白やタブのみを含むものも空行と判断される。）通常の段落は空白とタブのいずれによってもインデントされるべきではない。

"１つ以上の連続する行からなる"という定義の結果としてマークダウンは"ハードラップ"テキストの段落をサポートする。これは他のテキストをHTMLに変換する様式のほとんど（Movable Type の"Convert Line Breaks"も含む）が段落中の全ての改行文字を`<br />`タグに置き換えることとは著しく違う。

マークダウンを使うなかで *意図的に* `<br />`改行タグを挿入したいときは、行末に２つ以上のスペースを打ち、それからリターンを打ち込むこと。

そう、この方式は`<br />`を表現するのに少しだけ労力がかかる、しかし"全ての改行は`<br />`である"という定義はマークダウンでは通用しない。マークダウンのメール形式の[ブロッククオート][bq]や[リスト][l]は素晴らしく（そして見た目も良い）もしあなたが硬い改行の書式を使うのならば。

    [bq]: #blockquote
    [l]:  #list



<h3 id="header">見出し</h3>

マークダウンは２つの方式を見出しにおいて提供する、[Setext][1]と[atx][2]である。

Setext方式のヘッダは"下線"で示され、等号で最上位の見出しが、ダッシュで二位の見出しが表される。たとえば：

    これはH1であります
    ===============

    これはH2であります
    ---------------

`=`と`-`はいくつ連続させてもよい。

Atx方式の見出しは1〜6個のハッシュを行の先頭に配置し、それぞれが見出しのレベル1〜6に対応する。たとえば：

    # これはH1であります

    ## これはH2であります

    ###### これはH6であります

また任意で、atx方式の見出しを閉じることもできる。これはあくまでも装飾の域を出ず、閉じる方が見た目が良いと思うのならば使えばいい。閉じハッシュは開きハッシュの個数と同じである必要はない。(開きハッシュの数が見出しのレベルを決める。)：

    # これはH1であります #

    ## これはH2であります ##

    ### これはH3であります ######


<h3 id="blockquote">引用</h3>

マークダウンはメール方式の`>`を使い引用を表す。メールの引用方法に馴染みがあれば、マークダウンでの引用の書き方を知っているも同然だ。文をハードラップしてそれぞれの行頭に`>`をつけると最高に見た目が良い：

    > これは２つの段落からなる引用です。 その影響は単にその場限りでなくて、
    > 下車した後の数時間後までも継続する。それで近年難儀な慢性の病気にかかって以来、私は
    > 満員電車には乗らない事に、すいた電車にばかり乗る事に決めて、それを実行している。
    >
    > 必ずすいた電車に乗るために採るべき方法はきわめて平凡で簡単である。それはすいた
    > 電車の来るまで、気長く待つという方法である。

もし負担であるならばマークダウンはただ１つの`>`をハードラップ段落のつけるのみでも機能する：

    > その影響は単にその場限りでなくて、下車した後の数時間後までも継続する。それで近年難儀な慢性の病気にかかって以来、私は満員電車には乗らない事に、すいた電車にばかり乗る事に決めて、それを実行している。

    > 必ずすいた電車に乗るために採るべき方法はきわめて平凡で簡単である。それはすいた電車の来るまで、気長く待つという方法である。

引用はネストできる(すなわち引用の中にある引用)もう１つ`>`を付け加えることによって：

    > これは第一位の引用です
    >
    > > これはネストされた引用です
    >
    > 最初の位に戻ります

引用は他のマークダウン要素の、見出し、リスト、ブロックのコード などを含むことができる

	> ## これはヘッダです
	>
	> 1.   これは１つ目のリストアイテムです
	> 2.   これは２つ目のリストアイテムです
	>
	> ここにあるのはコードの例です
	>
	>     return shell_exec("echo $input | $markdown_script");

いずれの親切なテキストエディタはメール方式の引用をたやすく書き込めるはずだ。たとえば、BBEditにおいては、文章を選択することでメニューから引用の位をあげることができる。


<h3 id="list">リスト</h3>

マークダウンは順番のある(番号付き)と順番のない(箇条書き)のリストを提供する。

箇条書きリストは、アスタリスク、加算記号、ハイフン（どれでもよい）を行頭記号として使いる：

    *   あか
    *   みどり
    *   あお

これは次のものと同じであり：

    +   あか
    +   みどり
    +   あお

そしてこれとも：

    -   あか
    -   みどり
    -   あお

番号付きリストはピリオドに数字をつけて：

    1.  とりさん
    2.  まくはり
    3.  きょうく

注意すべきことに、リストに書いた数字はマークダウンが生成するHTMLに対してなんの効果もない。上記のマークダウンリストから生成されるHTMLは：

    <ol>
    <li>とりさん</li>
    <li>まくはり</li>
    <li>きょうく</li>
    </ol>

代わりにマークダウンリストをこのように書いたとしても

    1.  とりさん
    1.  まくはり
    1.  きょうく

もしくはこうでも

    3. とりさん
    1. まくはり
    8. きょうく

同じHTMLの出力を得ることができる。ポイントは、必要であるならば、番号付きマークダウンリストに順番に番号を振ることで、そのソースの番号が出力されるHTMLの番号に一致することである。しかし楽をしたいなら、する必要はない。

もし負担であるならば、全てのリストを1で始めればよい。将来マークダウンは任意の数から始める番号付きリストを提供するだろう。

行頭記号は一般的に左マージンから始まるが、3つ以上の空白でインデントすることもできる。行頭記号は１つ以上の空白かタブが後に続かなければならない。

リストの見た目をよくしたいなら、インデントで整えればよい：

    *   その影響は単にその場限りでなくて、下車した後の数時間後までも継続する。
        それで近年難儀な慢性の病気にかかって以来、私は満員電車には乗らない事に、
        すいた電車にばかり乗る事に決めて、それを実行している。
    *   必ずすいた電車に乗るために採るべき方法はきわめて平凡で簡単である。
        それはすいた電車の来るまで、気長く待つという方法である。

もし負担であるならば、必ずしもインデントをする必要はない：

    *   その影響は単にその場限りでなくて、下車した後の数時間後までも継続する。それで近年難儀な慢性の病気にかかって以来、私は満員電車には乗らない事に、すいた電車にばかり乗る事に決めて、それを実行している。
    *   必ずすいた電車に乗るために採るべき方法はきわめて平凡で簡単である。それはすいた電車の来るまで、気長く待つという方法である。

もしリストの要素が空行によって隔てられるとき、マークダウンは要素を`<p>`タグで包みHTMLに出力する。たとえば、次の入力では：

    *   とりさん
    *   まほう

こうなる：

    <ul>
    <li>とりさん</li>
    <li>まほう</li>
    </ul>

しかし次のようにすると：

    *   とりさん

    *   まほう

こうなる：

    <ul>
    <li><p>とりさん</p></li>
    <li><p>まほう</p></li>
    </ul>

リストの要素は複数の段落からなることができる。リスト要素に含まれるそれぞれの続く段落は空白4つもしくはタブによってインデントされなければならない。

    1.  これは２つの段落を内包するリスト要素です。 その影響は単にその場限り
        でなくて、下車した後の数時間後までも継続する。それで近年難儀な慢性の
        病気にかかって以来、

        私は満員電車には乗らない事に、すいた電車にばかり乗る事に決めて、
        それを実行している。必ずすいた電車に乗るために採るべき方法は
        きわめて平凡で簡単である。

    2.  それはすいた電車の来るまで、気長く待つという方法である。

全ての行をインデントするのは見た目がいい、しかし同様に、マークダウンは作業の負担を軽くする：

    *   これは２つの段落を内包するリスト要素です。

        リスト要素の２つ目の段落です。文頭のみをインデントすればよいのです。 その影響は単にその場限りでなくて、下車した後の数時間後までも継続する。

    *   同じリストの他の要素です。

引用をリストに含めるときは、区切り文字の`>`はインデントされなければならない：

    *   引用を内包するリスト要素:

        > これは引用です
        > リスト要素に内包されます。

ブロックのコードをリスト要素の中に書くとき、ブロックのコードは *2回* インデントされる必要がある、空白8つもしくはタブ２つ：

    *   ブロックのコードを内包するリスト要素です:

            <コードはここにあります>

注意すべきことに偶然で番号付きリストが生まれることがある、このように書くと：

    1986. なんと素晴らしいシーズン。

言い換えれば、*数字+ピリオド+空白* の順番で行が始まることである。これを避けるためには、ピリオドをバックスラッシュエスケープすればよい：

    1986\. なんと素晴らしいシーズン。



<h3 id="precode">ブロックのコード</h3>

事前に整えられたブロックのコードはプログラムやマークアップのソースコードを書くために使われる。通常の形態の段落と違い、ブロックのコードの行は書いた通りに解釈される。マークダウンはブロックのコードを`<pre>`と`<code>`タグで包む。

マークダウンでブロックのコードを作るためには、単純にそれぞれの行を少なくとも空白4つかタブ1つでインデントすればよい。たとえば、このような入力が与えられると：

    これは普通の段落です:

        これはブロックのコードです。

マークダウンは生成する：

    <p>これは普通の段落です:</p>

    <pre><code>これはブロックのコードです。
    </code></pre>

1レベルのインデント（空白4つかタブ1つ）はブロックのコードのそれぞれの行から削除される。たとえば、これは：

    AppleScriptでの例です:

        tell application "ほげ"
            beep
        end tell

こうなる：

    <p>AppleScriptでの例です:</p>

    <pre><code>tell application "ほげ"
        beep
    end tell
    </code></pre>

あるブロックのコードはインデントされていない行に行き着くまで有効である(もしくは記事が終わるまで)。

ブロックのコードの中において、アンド記号(`&`)と山括弧(`<`と`>`)は自動的に実体参照される。これはHTMLのソースコードの例をマークダウンに書き込むことをとても簡単にする、ペーストしてインデントするだけで、マークダウンは厄介なアンド記号と山括弧の翻訳をうまく処理する。たとえばこれは：

        <div class="footer">
            &copy; 2004年foo株式会社
        </div>

こうなる：

    <pre><code>&lt;div class="footer"&gt;
        &amp;copy; 2004年Foo株式会社
    &lt;/div&gt;
    </code></pre>

通常のマークダウン構文はブロックのコードの中では処理されない。例として、アンド記号はただの文字のアンド記号としてブロックのコードの中では扱われる。これは、マークダウンを使ってマークダウンの構文を書くことも簡単だということである。



<h3 id="hr">水平罫線</h3>

水平罫線タグ(`<hr />`)を表すためには3つ以上のハイフン、アスタリスク、下線符号 のみを任意の行に配置すればよい。望むのなら、ハイフンやアスタリスクの間に空白をいれてもよい。いずれの下に書かれた行も水平線を創出する：

    * * *

    ***

    *****

    - - -

    ---------------------------------------


* * *

<h2 id="span">インライン要素</h2>

<h3 id="link">リンク</h3>

マークダウンは２つの方式のリンクを提供する、*インライン* と *脚注* である。

双方の方式で、リンクの文は[角括弧]によって囲われる。

インライン方式のリンクを作るために、角括弧の直後に丸括弧のペアを書き込まなければならない。丸括弧の中には、リンクをさせたい地点のURLを記入し、ともに *追加* のリンクのタイトルをクオートで囲むことでつけることができる。たとえば：

    これは、[たとえの](http://example.com/ "タイトル")インラインリンクです。

    [このリンク](http://example.net/)はタイトル属性を持ちません。

翻訳されて：

    <p>これは、<a href="http://example.com/" title="タイトル">たとえの</a>インラインリンクです。</p>

    <p><a href="http://example.net/">このリンク</a>はタイトル属性を持ちません。</p>

同じサーバーにあるローカルな資源を参照したいときは、相対パスを使うことができる：

    私の[がいよう](/about/)ページを見れば詳細が書いてあります。

脚注方式のリンクは２つの角括弧のペアを使い、その中には特定するための任意のラベルを書き込む。：

    これは、[たとえの][id]脚注方式のリンクです。

任意で角括弧を空白で区切ってもよい：

    これは、[たとえの] [id]脚注方式のリンクです。

そして、文書のいずれの位置においても、任意の行を設定することで、次のようにリンクのラベルを定義することができる：

    [id]: http://example.com/  "追加のタイトルここ"

これは：

    *   角括弧はリンクの識別子を含む(任意で左マージンから空白３つまでインデントすることができる)
    *   コロンを引き続く
    *   １つ以上の空白が引き続く(タブでもよい)
    *   リンクのURLが引き続く
    *   任意で付属のタイトル属性をつけることができ、両端をシングルもしくはダブルの引用符で囲むか、丸括弧で囲む必要がある。

次の３つのリンクの定義は同様に作用する：

	[ほげ]: http://example.com/  "追加のタイトルここ"
	[ほげ]: http://example.com/  '追加のタイトルここ'
	[ほげ]: http://example.com/  (追加のタイトルここ)

**注意** マークダウン.pl 1.0.1 にはシングル引用符でリンクのタイトルをくくれないというバグが存在する。

リンクのURLは、もしそうしたければ、山括弧で括ることもできる：

    [id]: <http://example.com/>  "追加のタイトルここ"

タイトル属性を空白かタブでパディングした次の行に書くことができ、長いURLでよろしく見えることが多い：

    [id]: http://example.com/longish/path/to/resource/here
        "追加のタイトルここ"

定義リンクはマークダウンの処理にのみ使われ、出力されるHTMLからは取り除かれる。

定義リンクは、文字、数字、空白、句読点 を含むことができる、しかし大文字小文字を識別 *しない* 。例として次の２つのリンクは：

	[リンクのテキスト][a]
	[リンクのテキスト][A]

同じものである。

*暗黙のリンク名* はリンクの名前を書くことを省く手っ取り早い方法であり、リンクのテキストが名前と同じ時に有効である。何も含まない角括弧のペアを描けば良いだけであり --例として、"Google"という言葉を用いて google.com のウェブサイトにリンクさせようとするとき、単純に書くことができ：

	[Google][]

そしてリンクを定義すると：

	[Google]: http://google.com/

リンクの名前は空白を含むことがあるため、この手っ取り早い方法は複数の単語がリンクのテキストに含まれている場合でも有効である：

	詳しくは[Daring Fireball][]を見てください。

そしてリンクを定義すると：

	[Daring Fireball]: http://daringfireball.net/

定義リンクは マークダウン 文書のいずれの位置に書くこともできる。私はそれらが使われた段落の直後に書き込むことにしているが、もし必要であれば、脚注のように、全てを文書の末尾に記載することも可能である。

それでは実践的な脚注リンクの例をあげよう：

    私は10倍以上の検索流入を[Google][1]から[Yahoo][2]や[MSN][3]からのものと比較して得ました。

      [1]: http://google.com/        "Google"
      [2]: http://search.yahoo.com/  "Yahoo Search"
      [3]: http://search.msn.com/    "MSN Search"

暗黙のリンク名を使って手っ取り早くやると、代わりにこのように書ける：

    私は10倍以上の検索流入を[Google][]から[Yahoo][]や[MSN][]からのものと比較して得ました。

      [google]: http://google.com/        "Google"
      [yahoo]:  http://search.yahoo.com/  "Yahoo Search"
      [msn]:    http://search.msn.com/    "MSN Search"

上の２つの例は双方ともに下のHTMLを出力する：

    <p>私は10倍以上の検索流入を<a href="http://google.com/" title="Google">Google</a>から<a href="http://search.yahoo.com/" title="Yahoo Search">Yahoo</a>や<a href="http://search.msn.com/" title="MSN Search">MSN</a>からのものと比較して得ました。</p>

比較として、同じ段落をマークダウン方式のインライン方式のリンクを使って書いたものが：

    私は10倍以上の検索流入を[Google](http://google.com/ "Google")から[Yahoo](http://search.yahoo.com/ "Yahoo Search")や[MSN](http://search.msn.com/ "MSN Search")からのものと比較して得ました。

脚注方式のリンクで重要なことは、書くのが簡単ではないということだ。重要なことは脚注方式のリンクが、はるかに文書のソースを読みやすくするということである。上記の例と比べると、脚注方式のリンクは段落自体に81文字しか含まず、インライン方式のリンクは176文字になり、HTMLでは234文字となる。HTMLでは、よりマークアップされることになる。

マークダウンの脚注方式のリンクは、文書のソースを、ブラウザで描画される最終的な出力により近づけることができる。マークアップに関わるメタデータを段落の外に置けることは、散文を綴る速度を落とさずにリンクを追加できることを意味する。


<h3 id="em">強調</h3>

マークダウンはアスタリスク(`*`)と下線符号(`_`)を強調の指標として用いる。文が１つの`*`か`_`で挟まれるとHTMLの`<em>`タグで挟まれ；２つの`*`か`_`で挟まれるとHTMLの`<strong>`で挟まれる。例として、次のような入力は：

    *アスタリスク１つ*

    _下線符号１つ_

    **アスタリスク２つ**

    __下線符号2つ__

こうなる：

    <em>アスタリスク１つ</em>

    <em>下線符号１つ</em>

    <strong>アスタリスク１つ</strong>

    <strong>下線符号2つ</strong>

どちらでも好きな方を使えばよく、唯一の制約は強調する区域は同じ文字で開始終了しなければならないことだけである。

強調は単語の中で利用することができる：

    まったく*べらぼうに*信じられません

しかし`*`や`_`を空白で囲むと、文字通りのアスタリスクと下線符号として解釈される。

強調子の位置に文字としてのアスタリスクや下線符号を書き込みたいときは、バックスラッシュでエスケープすれば良い：

    \*この文章は文字通りのアスタリスクで囲まれています\*



<h3 id="code">コード</h3>

インラインのコードの範囲を指し示すためには、バッククオート(`` ` ``)を使う。整えられたブロックのコードと違い、インラインのコードは通常の段落の中に含まれる。たとえば：

    関数の`printf()`を使えばよろしいのです。

はこうなる：

    <p>関数の <code>printf()</code>を使えばよろしいのです。</p>

文字通りのバッククオートをインラインのコードの中で使いたいときは、複数の連続したバッククオートを開始終了の範囲決定子に使えばよい：

    ``こちらにあるのは(`)文字通りのバッククートです``

これはこうなる：

    <p><code>こちらにあるのは(\`)文字通りのバッククートです</code></p>

バッククオート範囲決定子はもし空白を含んでいるときはインラインのコードによって囲われる、開く前に一個と、閉じた後に一個。これはただの文字としてのバッククオートをインラインのコードの最初または最後に書き込むことを可能にする：

	１つのバッククオートがインラインのコードの中にあります: `` ` ``

	インラインのコードの中でバッククオートが文字の範囲を決定しています: `` `ほげ` ``

変換されて：

	<p>１つのバッククオートがインラインコードの中にあります: <code>\`</code></p>

	<p>インラインコードの中でバッククオートが文字の範囲を決定しています: <code>`ほげ`</code></p>

インラインコードは、アンド記号と山括弧は自動的に実体参照され、これはHTMLタグを書き入れることを簡単にする。マークダウンはこれを変換して：

    後生だから`<blink>`タグを使わないでください。

こうなる：

    <p>後生だから<code>&lt;blink&gt;</code>タグを使わないでください。</p>

このように書いたら：

    `&#8212;`は十進法で表現されており`&mdash;`を表します。

こう変換される：

    <p><code>&amp;#8212;</code>は十進法で表現されており<code>&amp;mdash;</code> を表します。</p>



<h3 id="img">画像</h3>

確かに、プレーンテキスト形式の文書に画像を置くための"自然な"構文を考え出すことは極めて難しい。

マークダウンにおける画像の構文はリンクの構文と似たものにするつもりであり、*インライン* と *脚注* の２つの方式で書くことができる。

インライン方式の画像構文はこのようになる：

    ![代替の文章](/path/to/img.jpg)

    ![代替の文章](/path/to/img.jpg "追加のタイトル")

これは：

*   感嘆符`!`
*   引き続く角括弧のペア、画像の`代替`属性の文章を含む
*   引き続く丸括弧のペア、画像へのURLかパスを含む、追加の`title`要素はシングルもしくはダブル引用符で囲まれる。

脚注方式の画像構文はこのようになる：

    ![代替の文章][id]

"id"とは画像の脚注を見つけるための手がかりとなる識別子である。画像の脚注はリンクの脚注と一致することを用いて定義される。

    [id]: url/to/image  "追加のタイトル属性"

この書き方においては、マークダウンは画像の大きさを指定する構文を持たないため、もし大きさの情報が重要であるなら、単純にHTMLの`<img>`タグを使うことで解決する。


* * *


<h2 id="misc">その他</h2>

<h3 id="autolink">自動リンク</h3>

マークダウンは作業の負担を軽減するURLやメールの"自動的な"リンクへの変換機能を提供する、単純にURLやメールを山括弧で囲むことで。これが何を意味するかというと実際のURLやメールのアドレスを表示したく、それらをクリックできるリンクとしたいとき、こうすることができる：

    <http://example.com/>

マークダウンはこれを翻訳して：

    <a href="http://example.com/">http://example.com/</a>

自動リンクはメールアドレスについて同様に働くが、違う動作をすることにマークダウンアドレス収集スパムボットに発見されないよう十進法と六進法の実体参照を用いて少しだけ難読化する。たとえば、マークダウンはこれを翻訳して：

    <address@example.com>

このようにする：

    <a href="&#x6D;&#x61;i&#x6C;&#x74;&#x6F;:&#x61;&#x64;&#x64;&#x72;&#x65;&#115;&#115;&#64;&#101;&#120;&#x61;&#109;&#x70;&#x6C;e&#x2E;&#99;&#111;&#109;">&#x61;&#x64;&#x64;&#x72;&#x65;&#115;&#115;&#64;&#101;&#120;&#x61;&#109;&#x70;&#x6C;e&#x2E;&#99;&#111;&#109;</a>

これはブラウザにおいて"address@example.com"というクリック可能なリンクとして描画される。

(この種類のエンティティ符号化は多くのアドレス収集ボットに対して、たとえ全てのボットに対してではないにしても、実に愚かな選択であるが、間違いなく全てのボットに対してではない。何もないよりはマシで、しかしこの方法で公開されたアドレスでさえもゆくゆくはスパムが手に入れることになる。)



<h3 id="backslash">バックスラシュエスケープ</h3>

マークダウンはバックスラッシュでエスケープすることによって マークダウン形式の構文では特別な意味を持つ文字をそのまま文字通り表すことができる。たとえば、文字としてのアスタリスクで文を囲みたいとき(HTMLの`<em>`タグではなく)、アスタリスクの前にバックスラッシュを入れることができる、このように：

    \*literal asterisks\*

マークダウンは次の文字についてバックスラッシュエスケープを提供する：

    \   バックスラッシュ
    `   バッククオート
    *   アスタリスク
    _   下線符号
    {}  波括弧
    []  角括弧
    ()  丸括弧
    #   ハッシュ
	+	プラス記号
	-	マイナス記号(ハイフン)
    .   点
    !   感嘆符
